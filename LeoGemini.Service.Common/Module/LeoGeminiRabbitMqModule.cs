﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EventBus.RabbitMq;
using Volo.Abp.Modularity;
using Volo.Abp.RabbitMQ;

namespace LeoGemini.Service.Common.Module
{
    [DependsOn(
        typeof(AbpEventBusRabbitMqModule))]
    public class LeoGeminiRabbitMqModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var services = context.Services;
            var configuration = services.GetConfiguration();
            
            // 配置rabbit mq
            Configure<AbpRabbitMqOptions>(options =>
            {
                options.Connections.Default.ClientProvidedName = configuration.GetValue<string>("RabbitMQ:EventBus:ClientName");
                options.Connections.Default.RequestedHeartbeat = new TimeSpan(configuration.GetValue<int>("RabbitMQ:EventBus:Heartbeat"));
                options.Connections.Default.UserName = configuration.GetValue<string>("RabbitMQ:EventBus:User");
                options.Connections.Default.Password = configuration.GetValue<string>("RabbitMQ:EventBus:Pwd");
            });
        }
    }
}