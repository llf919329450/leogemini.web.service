using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using UserService.EntityFrameworkCore;

namespace UserService.HttpApi.Hosting
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplication<UserServiceHttpApiHostingModule>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserServiceDbContext context)
        {
            app.InitializeApplication();
            context.Database.Migrate();
        }
    }
}