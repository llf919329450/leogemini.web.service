﻿using System;
using System.Threading.Tasks;
using LeoGemini.Service.Infrastructure.Constract;
using Microsoft.AspNetCore.Mvc;
using UserService.Application.Constract.DTO.Input;
using UserService.Application.Constract.IAppService;

namespace UserService.HttpApi.Hosting.Controller
{
    [Route("api/v1/users")]
    public class UsersController : ApiResponseController
    {
        private readonly IUserAppService _userAppService;

        public UsersController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        /// <summary>
        /// 用户注册
        /// </summary>
        [HttpPost("join")]
        public async Task<object> CreateNewUser(CreateUserInputDto dto)
        {
            return await _userAppService.CreateUserAsync(dto);
        }

        /// <summary>
        /// 用户登陆
        /// </summary>
        [HttpPost("login")]
        public async Task<object> UserLogin(UserLoginInputDto dto)
        {
            return await _userAppService.UserLoginAsync(dto);
        }
        
        /// <summary>
        /// 获取用户信息
        /// </summary>
        [HttpGet("{userId}/profile")]
        public async Task<object> GetUserByUserId(Guid userId)
        {
            return await _userAppService.GetUserByUserIdAsync(userId);
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        [HttpPut("{userId}/profile/edit")]
        public async Task<object> EditUserProfileByUserId(Guid userId, EditUserInputDto dto)
        {
            return await _userAppService.EditUserByUserIdAsync(userId, dto);
        }

        /// <summary>
        /// 用户获取配送地址信息
        /// </summary>
        [HttpGet("{userId}/addresses")]
        public async Task<object> GetUserAddressesByUserId(Guid userId)
        {
            return await _userAppService.GetAddressesByUserIdAsync(userId);
        }

        /// <summary>
        /// 通过用户id和配送地址id获取配送地址信息
        /// </summary>
        [HttpGet("{userId}/addresses/{addressId}")]
        public async Task<object> GetUserAddressByUserIdAndAddressId(Guid userId, Guid addressId)
        {
            return await _userAppService.GetAddressByUserIdAndAddressIdAsync(userId, addressId);
        }

        /// <summary>
        /// 用户新增配送地址信息
        /// </summary>
        [HttpPost("{userId}/addresses/create")]
        public async Task<object> CreateUserAddress(Guid userId, CreateAddressInputDto dto)
        {
            return await _userAppService.CreateAddressAsync(userId, dto);
        }

        /// <summary>
        /// 用户修改配送地址信息
        /// </summary>
        [HttpPut("{userId}/addresses/{addressId}/edit")]
        public async Task<object> EditUserAddressByUserIdAndAddressId(Guid userId, Guid addressId, EditAddressInputDto dto)
        {
            return await _userAppService.EditAddressAsync(userId, addressId, dto);
        }

        /// <summary>
        /// 用户删除配送地址信息
        /// </summary>
        [HttpDelete("{userId}/addresses/{addressId}/delete")]
        public async Task DeleteUserAddressByUserIdAndAddressId(Guid userId, Guid addressId)
        {
            await _userAppService.DeleteAddressAsync(userId, addressId);
        }

        /// <summary>
        /// 用户请求验证邮箱
        /// </summary>
        [HttpPost("{userId}/trust/email")]
        public async Task RequireToTrustEmail(Guid userId)
        {
            await _userAppService.PublishUserRequireToTrustEmailEventAsync(userId);
        }

        /// <summary>
        /// 用户确认邮箱
        /// </summary>
        [HttpPut("{userId}/email/confirmed")]
        public async Task RequireToTrustedEmail(Guid userId, ConfirmMailInputDto dto)
        {
            await _userAppService.RequireToTrustedEmailAsync(userId, dto);
        }
        
        /// <summary>
        /// 用户请求验证手机号
        /// </summary>
        [HttpPost("{userId}/trust/phone")]
        public async Task RequireToTrustPhone(Guid userId)
        {
            await _userAppService.PublishUserRequireToTrustPhoneNumEventAsync(userId);
        }
    }
}