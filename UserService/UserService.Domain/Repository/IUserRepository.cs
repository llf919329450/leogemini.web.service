﻿using System;
using LeoGemini.Service.Infrastructure.Constract;
using UserService.Domain.Aggregate.User;
using Volo.Abp.Domain.Repositories;

namespace UserService.Domain.Repository
{
    /// <summary>
    /// 用户仓储接口
    /// </summary>
    public interface IUserRepository : IRepository<User, Guid>
    {
    }
}