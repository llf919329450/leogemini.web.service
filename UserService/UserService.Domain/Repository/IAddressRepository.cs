﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LeoGemini.Service.Infrastructure.Constract;
using UserService.Domain.Aggregate.Address;
using Volo.Abp.Domain.Repositories;

namespace UserService.Domain.Repository
{
    /// <summary>
    /// 用户配送地址仓储接口
    /// </summary>
    public interface IAddressRepository : IRepository<Address, Guid>
    {
        
    }
}