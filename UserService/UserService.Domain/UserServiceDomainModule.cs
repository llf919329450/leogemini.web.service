﻿using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace UserService.Domain
{
    [DependsOn(typeof(AbpIdentityDomainModule))]
    public class UserServiceDomainModule : AbpModule
    {
    }
}