﻿using System;
using Volo.Abp.EventBus;

namespace UserService.Domain.Event
{
    /// <summary>
    /// 用户请求信任手机号登陆
    /// </summary>
    [EventName("LeoGee.User.RequireToTrustPhone")]
    public class RequireToTrustPhoneEvent
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { get; set; }
        
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        public string VerifyCode { get; set; }
    }
}