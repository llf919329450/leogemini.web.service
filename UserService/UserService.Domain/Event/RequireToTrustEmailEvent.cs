﻿using System;
using Volo.Abp.EventBus;

namespace UserService.Domain.Event
{
    /// <summary>
    /// 用户请求信任邮箱登陆
    /// </summary>
    [EventName("LeoGee.User.RequireToTrustEmail")]
    public class RequireToTrustEmailEvent
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { get; set; }
        
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        public string VerifyCode { get; set; }
    }
}