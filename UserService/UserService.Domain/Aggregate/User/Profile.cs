﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using UserService.Domain.Shared.Enum;
using Volo.Abp.Domain.Entities;

namespace UserService.Domain.Aggregate.User
{
    /// <summary>
    /// 用户详情
    /// </summary>
    [Table("tb_user_profile")]
    public class Profile : Entity<Guid>
    {
        /// <summary>
        /// ORM框架使用
        /// </summary>
        protected Profile() { }

        public Profile(Guid userId)
        {
            Id = userId;
            Intro = "这个人很懒，什么也没留下~";
            Sex = UserSex.Man;
        }

        public Profile(Guid userId, DateTime birthDate) : this(userId)
        {
            BirthDate = birthDate;
        }
        
        /// <summary>
        /// 个人说明
        /// </summary>
        [Column("intro")]
        public virtual string Intro { get; internal set; }
        
        /// <summary>
        /// 头像
        /// </summary>
        [Column("avatar")]
        public virtual string Avatar { get; internal set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        [Column("sex")]
        public virtual UserSex Sex { get; internal set; }
        
        /// <summary>
        /// 生日
        /// </summary>
        [Column("birth_date")]
        public virtual DateTime BirthDate { get; internal set; }
        
        /// <summary>
        /// 用户导航属性
        /// </summary>
        [JsonIgnore]
        public User User { get; private set; }
    }
}