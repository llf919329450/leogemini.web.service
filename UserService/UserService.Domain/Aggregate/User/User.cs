﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;
using LeoGemini.Service.Infrastructure.Extension;
using UserService.Domain.Shared.Enum;

namespace UserService.Domain.Aggregate.User
{
    /// <summary>
    /// 用户聚合根
    /// </summary>
    [Table("tb_user")]
    public class User : AggregateRoot<Guid>, IHasCreationTime
    {
        /// <summary>
        /// ORM框架使用
        /// </summary>
        protected User() { }

        public User(Guid id)
        {
            Id = id;
            IsEmailConfirmed = false;
            IsPhoneNumConfirmed = false;
            Salt = new Random().Next(6, Extensions.MixinType.Mixin);
        }

        public User(Guid id, string userName, string email, string phoneNum, string password, bool allowedNotification) : this(id)
        {
            UserName = userName;
            Email = email;
            PhoneNum = phoneNum;
            AllowedNotification = allowedNotification;
            ChangePassword(password);
        }
        
        /// <summary>
        /// 用户名
        /// </summary>
        [Column("user_name")]
        public virtual string UserName { get; private set; }
        
        /// <summary>
        /// 邮箱
        /// </summary>
        [Column("email")]
        public virtual string Email { get; private set; }
        
        /// <summary>
        /// 邮箱状态
        /// </summary>
        [Column("is_email_confirmed")]
        public virtual bool IsEmailConfirmed { get; private set; }
        
        /// <summary>
        /// 手机号
        /// </summary>
        [Column("phone_num")]
        public virtual string PhoneNum { get; private set; }
        
        /// <summary>
        /// 手机号状态
        /// </summary>
        [Column("is_phone_num_confirmed")]
        public virtual bool IsPhoneNumConfirmed { get; private set; }
        
        /// <summary>
        /// 密码
        /// </summary>
        [Column("password")]
        public virtual string Password { get; private set; }
        
        /// <summary>
        /// 盐
        /// </summary>
        [Column("salt")]
        public virtual string Salt { get; private set; }
        
        /// <summary>
        /// 通知标识
        /// </summary>
        [Column("allowed_notification")]
        public virtual bool AllowedNotification { get; private set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        [Column("creation_time")]
        public virtual DateTime CreationTime { get; set; }
        
        /// <summary>
        /// 详细信息
        /// </summary>
        public virtual Profile Profile { get; private set; }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="password">新密码</param>
        public void ChangePassword(string password)
        {
            Password = (Salt + password).ToMd5();
        }

        /// <summary>
        /// 初始化用户详情
        /// </summary>
        public void InitProfile(Profile profile)
        {
            Profile = profile;
        }

        /// <summary>
        /// 修改详细信息
        /// </summary>
        public void ChangeProfile(string intro, string avatar, UserSex sex, DateTime birth)
        {
            Profile.Intro = intro;
            Profile.Avatar = avatar;
            Profile.Sex = sex;
            Profile.BirthDate = birth;
        }

        /// <summary>
        /// 确认邮件
        /// </summary>
        public void ConfirmEmail()
        {
            IsEmailConfirmed = true;
        }

        /// <summary>
        /// 确认手机号
        /// </summary>
        public void ConfirmPhoneNum()
        {
            IsPhoneNumConfirmed = true;
        }
    }
}