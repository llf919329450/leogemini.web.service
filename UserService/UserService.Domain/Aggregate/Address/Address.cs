﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;

namespace UserService.Domain.Aggregate.Address
{
    /// <summary>
    /// 用户配送地址
    /// </summary>
    [Table("tb_user_address")]
    public class Address : AggregateRoot<Guid>, IHasCreationTime
    {
        protected Address() { }

        public Address(Guid id)
        {
            Id = id;
        }

        public Address(Guid id, Guid userId, string contact, string phoneNum, string province, string city, string region, string details) : this(id)
        {
            ReferenceUserId = userId;
            Contact = contact;
            PhoneNum = phoneNum;
            Province = province;
            City = city;
            Region = region;
            Details = details;
        }
        
        /// <summary>
        /// 关联用户
        /// </summary>
        [Column("reference_user_id")]
        public virtual Guid ReferenceUserId { get; private set; }
        
        /// <summary>
        /// 联系人
        /// </summary>
        [Column("contact")]
        public virtual string Contact { get; internal set; }
        
        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("phone_num")]
        public virtual string PhoneNum { get; internal set; }
        
        /// <summary>
        /// 省份
        /// </summary>
        [Column("province")]
        public virtual string Province { get; internal set; }
        
        /// <summary>
        /// 城市
        /// </summary>
        [Column("city")]
        public virtual string City { get; internal set; }
        
        /// <summary>
        /// 地区
        /// </summary>
        [Column("region")]
        public virtual string Region { get; internal set; }
        
        /// <summary>
        /// 详情
        /// </summary>
        [Column("details")]
        public virtual string Details { get; internal set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("creation_time")]
        public virtual DateTime CreationTime { get; set; }

        /// <summary>
        /// 地址变更
        /// </summary>
        public void Migrate(string contact, string phoneNum, string province, string city, string region, string details)
        {
            Contact = contact;
            PhoneNum = phoneNum;
            Province = province;
            City = city;
            Region = region;
            Details = details;
        }
    }
}