﻿using System;
using UserService.Domain.Aggregate.User;

namespace UserService.Domain.Service
{
    public interface IUserManager
    {
        User CreateUser(string userName, string email, string phoneNum, string password, DateTime birth, bool allowedNotification);
    }
}