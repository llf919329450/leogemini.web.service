﻿using System;
using UserService.Domain.Aggregate.User;
using Volo.Abp.Domain.Services;

namespace UserService.Domain.Service.Impl
{
    /// <summary>
    /// 用户领域服务
    /// </summary>
    public class UserManager : DomainService, IUserManager
    {
        /// <summary>
        /// 创建新用户
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="email">邮箱</param>
        /// <param name="phoneNum">手机号</param>
        /// <param name="password">密码</param>
        /// <param name="birth">生日</param>
        /// <param name="allowedNotification">是否接收消息推送</param>
        public User CreateUser(string userName, string email, string phoneNum, string password, DateTime birth,
            bool allowedNotification)
        {
            // 创建用户
            var user = new User(
                GuidGenerator.Create(),
                userName, email, phoneNum, password, allowedNotification);

            // 创建用户详情
            var profile = new Profile(user.Id, birth)
            {
                Avatar = "avatar/default.png"
            };
            
            // 初始化用户详情
            user.InitProfile(profile);

            return user;
        }
    }
}