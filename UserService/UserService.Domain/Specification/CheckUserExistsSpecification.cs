﻿using System;
using System.Linq.Expressions;
using UserService.Domain.Aggregate.User;
using Volo.Abp.Specifications;

namespace UserService.Domain.Specification
{
    /// <summary>
    /// 用户规约 - 检查用户是否存在
    /// </summary>
    public class CheckUserExistsSpecification : Specification<User>
    {
        private readonly string _compareAccount;
        
        public CheckUserExistsSpecification(string compareAccount)
        {
            _compareAccount = compareAccount;
        }
        
        public override Expression<Func<User, bool>> ToExpression()
        {
            return user => 
                user.UserName == _compareAccount || 
                user.PhoneNum == _compareAccount ||
                user.Email == _compareAccount;
        }
    }
}