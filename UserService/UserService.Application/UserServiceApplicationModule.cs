﻿using LeoGemini.Service.Common.Module;
using LeoGemini.Service.Infrastructure;
using UserService.Domain;
using UserService.EntityFrameworkCore;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace UserService.Application
{
    [DependsOn(
        typeof(UserServiceDomainModule),
        typeof(UserServiceEntityFrameworkCoreModule),
        typeof(AbpAutoMapperModule),
        typeof(LeoGeminiFreeRedisModule),
        typeof(LeoGeminiRabbitMqModule),
        typeof(LeoGeminiConsulModule),
        typeof(LeoGeminiServiceInfrastructureModule))]
    public class UserServiceApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddProfile<UserServiceAutoMapperProfile>(validate: true);
            });
        }
    }
}