﻿using System.Threading.Tasks;
using UserService.Application.Constract.IAppService;
using UserService.Domain.Aggregate.User;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities.Events;
using Volo.Abp.EventBus;

namespace UserService.Application.EventHandler
{
    /// <summary>
    /// 处理用户注册事件
    /// </summary>
    public class UserRegisteredEventHandler : ILocalEventHandler<EntityCreatedEventData<User>>, ITransientDependency
    {
        private readonly IUserAppService _userAppService;

        public UserRegisteredEventHandler(
            IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }
        
        /// <summary>
        /// 事件处理程序
        /// </summary>
        public async Task HandleEventAsync(EntityCreatedEventData<User> eventData)
        {
            // 调用应用服务完成事件分发
            await _userAppService.PublishUserRequireToTrustEmailEventAsync(eventData.Entity.Id);
        }
    }
}