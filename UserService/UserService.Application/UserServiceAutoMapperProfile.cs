﻿using UserService.Application.Constract.DTO.Output;
using UserService.Domain.Aggregate.Address;
using UserService.Domain.Aggregate.User;
using Profile = AutoMapper.Profile;
using UserProfile = UserService.Domain.Aggregate.User.Profile;

namespace UserService.Application
{
    public class UserServiceAutoMapperProfile : Profile
    {
        public UserServiceAutoMapperProfile()
        {
            CreateMap<User, CreateUserReturnDto>();
            CreateMap<UserProfile, CreateProfileReturnDto>();
            CreateMap<Address, CreateAddressReturnDto>();
        }
    }
}