﻿// ReSharper disable InconsistentNaming
namespace UserService.Application
{
    public static class ErrorMessage
    {
        public const string USER_USERNAME_NOT_EXIST = "用户名不存在";
        public const string USER_EMAIL_NOT_TRUSTED = "用户暂未信任邮箱登陆";
        public const string USER_PHONE_NOT_TRUSTED = "用户暂未信任手机号码登陆";
        public const string USER_PASSWORD_ERROR = "密码错误";
        public const string USER_USERNAME_EXIST = "用户名已存在";
        public const string USER_EMAIL_EXIST = "邮箱已存在";
        public const string USER_PHONENUM_EXIST = "手机号码已存在";
        public const string USER_NO_EXIST = "用户不存在";
        public const string USER_HAVE_NOT_PERMISSION = "你没有权限进行此操作";
        public const string USER_DONT_HAVE_THIS_REQUEST = "指定用户没有需要校验的请求";
        public const string USER_VERIFY_DO_NOT_COMPARE = "验证码不匹配";

        public const string ADDRESS_COMPARE_ERROR = "配送地址id不一致";
        
        
    }
}