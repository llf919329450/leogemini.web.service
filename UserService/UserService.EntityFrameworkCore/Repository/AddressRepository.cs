﻿using System;
using UserService.Domain.Aggregate.Address;
using UserService.Domain.Repository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace UserService.EntityFrameworkCore.Repository
{
    public class AddressRepository : EfCoreRepository<UserServiceDbContext, Address, Guid>, IAddressRepository
    {
        public AddressRepository(IDbContextProvider<UserServiceDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}