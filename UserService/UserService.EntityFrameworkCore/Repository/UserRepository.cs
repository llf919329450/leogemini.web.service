﻿using System;
using UserService.Domain.Aggregate.User;
using UserService.Domain.Repository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace UserService.EntityFrameworkCore.Repository
{
    public class UserRepository : EfCoreRepository<UserServiceDbContext, User, Guid>, IUserRepository 
    {
        public UserRepository(IDbContextProvider<UserServiceDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}