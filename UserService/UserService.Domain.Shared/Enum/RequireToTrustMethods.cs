﻿namespace UserService.Domain.Shared.Enum
{
    /// <summary>
    /// 登陆方式
    /// </summary>
    public enum RequireToTrustMethods
    {
        /// <summary>
        /// 邮箱登陆
        /// </summary>
        Email, 
        
        /// <summary>
        /// 电话号码登陆
        /// </summary>
        PhoneNum
    }
}