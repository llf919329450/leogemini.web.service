﻿namespace UserService.Domain.Shared.Enum
{
    /// <summary>
    /// 用户性别
    /// </summary>
    public enum UserSex
    {
        /// <summary>
        /// 男
        /// </summary>
        Man, 
        
        /// <summary>
        /// 女
        /// </summary>
        Woman
    }
}