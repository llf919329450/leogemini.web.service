﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UserService.Application.Constract.DTO.Input
{
    public class CreateUserInputDto
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Required(ErrorMessage = "用户名不能为空")]
        public string UserName { get; set; }
        
        /// <summary>
        /// 手机号
        /// </summary>
        [Required(ErrorMessage = "手机号不能为空")]
        public string PhoneNum { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        [Required(ErrorMessage = "出生日期不能为空")]
        public DateTime BirthDate { get; set; }
        
        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "用户密码不能为空")]
        public string Password { get; set; }
        
        /// <summary>
        /// 邮箱
        /// </summary>
        [Required(ErrorMessage = "邮箱不能为空")]
        public string Email { get; set; }

        /// <summary>
        /// 接收推送
        /// </summary>
        public bool AllowedNotification { get; set; } = false;
    }
}