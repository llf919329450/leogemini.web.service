﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UserService.Application.Constract.DTO.Input
{
    public class ConfirmMailInputDto
    {
        [Required(ErrorMessage = "用户id不为空")]
        public Guid UserId { get; set; }
        
        [Required(ErrorMessage = "验证码不为空")]
        public string VerifyCode { get; set; }
    }
}