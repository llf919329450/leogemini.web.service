﻿namespace UserService.Application.Constract.DTO.Input
{
    /// <summary>
    /// 用户登陆dto
    /// </summary>
    public class UserLoginInputDto
    {
        /// <summary>
        /// 用户账号
        /// </summary>
        public string Account { get; set; }
        
        /// <summary>
        /// 登陆密码
        /// </summary>
        public string Password { get; set; }
    }
}