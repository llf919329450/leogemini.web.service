﻿using System;

namespace UserService.Application.Constract.DTO.Output
{
    public class CreateAddressReturnDto
    {
        /// <summary>
        /// 配送地址id
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid ReferenceUserId { get; set; }
        
        /// <summary>
        /// 联系人
        /// </summary>
        public string Contact { get; set; }
        
        /// <summary>
        /// 联系电话
        /// </summary>
        public string PhoneNum { get; set; }
        
        /// <summary>
        /// 省份
        /// </summary>
        public string Province { get; set; }
        
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        
        /// <summary>
        /// 地区
        /// </summary>
        public string Region { get; set; }
        
        /// <summary>
        /// 详情
        /// </summary>
        public string Details { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }
    }
}