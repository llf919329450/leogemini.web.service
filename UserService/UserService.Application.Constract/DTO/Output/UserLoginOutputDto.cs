﻿namespace UserService.Application.Constract.DTO.Output
{
    /// <summary>
    /// 用户登陆返回dto
    /// </summary>
    public class UserLoginOutputDto
    {
        /// <summary>
        /// 登陆token
        /// </summary>
        public string Token { get; set; }
    }
}