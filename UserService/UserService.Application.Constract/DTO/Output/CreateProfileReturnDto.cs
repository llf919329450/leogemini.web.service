﻿using System;
using UserService.Domain.Shared.Enum;

namespace UserService.Application.Constract.DTO.Output
{
    public class CreateProfileReturnDto
    {
        /// <summary>
        /// 生日
        /// </summary>
        public DateTime BirthDate { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public UserSex Sex { get; set; }
        
        /// <summary>
        /// 个人简介
        /// </summary>
        public string Intro { get; set; }
        
        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }
    }
}