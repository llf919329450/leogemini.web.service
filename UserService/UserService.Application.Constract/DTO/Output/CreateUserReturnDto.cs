﻿using System;

namespace UserService.Application.Constract.DTO.Output
{
    public class CreateUserReturnDto
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid Id { get; set; }
                
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNum { get; set; }
        
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }
        
        public CreateProfileReturnDto Profile { get; set; }
    }
}