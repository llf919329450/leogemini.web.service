﻿namespace UserService.Application.Constract
{
    public static class VerifyAction
    {
        public const string VERIFY_EMAIL = "verify_email";
        public const string VERIFY_PHONE = "verify_phone";
    }
}