﻿using System;

namespace UserService.Application.Constract.IAppService
{
    public interface IVerifyCodeManager
    {
        /// <summary>
        /// 生成验证码
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <param name="verifyAction">枚举自VerifyAction</param>
        string GenerateVerifyCode(Guid userId, string verifyAction);

        /// <summary>
        /// 获取已生成的用户验证码
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <param name="verifyAction">枚举自VerifyAction</param>
        string GetGeneratedVerifyCode(Guid userId, string verifyAction);

        /// <summary>
        /// 销毁已生成的用户验证码
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <param name="verifyAction">枚举自VerifyAction</param>
        void DestroyGeneratedVerifyCode(Guid userId, string verifyAction);
    }
}