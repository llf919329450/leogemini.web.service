﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserService.Application.Constract.DTO.Input;
using UserService.Application.Constract.DTO.Output;

namespace UserService.Application.Constract.IAppService
{
    /// <summary>
    /// 用户应用服务接口
    /// </summary>
    public interface IUserAppService
    {
        /// <summary>
        /// 用户注册
        /// </summary>
        Task<CreateUserReturnDto> CreateUserAsync(CreateUserInputDto dto);

        /// <summary>
        /// 用户登陆
        /// </summary>
        Task<UserLoginOutputDto> UserLoginAsync(UserLoginInputDto dto);

        /// <summary>
        /// 获取用户详情
        /// </summary>
        Task<CreateUserReturnDto> GetUserByUserIdAsync(Guid userId);

        /// <summary>
        /// 修改用户详情
        /// </summary>
        Task<CreateUserReturnDto> EditUserByUserIdAsync(Guid userId, EditUserInputDto dto);

        /// <summary>
        /// 用户获取配送地址信息
        /// </summary>
        Task<ICollection<CreateAddressReturnDto>> GetAddressesByUserIdAsync(Guid userId);

        /// <summary>
        /// 用户新增配送地址信息
        /// </summary>
        Task<CreateAddressReturnDto> CreateAddressAsync(Guid userId, CreateAddressInputDto dto);

        /// <summary>
        /// 用户修改配送地址信息
        /// </summary>
        Task<CreateAddressReturnDto> EditAddressAsync(Guid userId, Guid addressId, EditAddressInputDto dto);

        /// <summary>
        /// 用户删除配送地址信息
        /// </summary>
        Task DeleteAddressAsync(Guid userId, Guid addressId);

        /// <summary>
        /// 通过用户id和配送地址id获取配送地址信息
        /// </summary>
        Task<CreateAddressReturnDto> GetAddressByUserIdAndAddressIdAsync(Guid userId, Guid addressId);

        /// <summary>
        /// 用户请求验证邮箱
        /// </summary>
        Task PublishUserRequireToTrustEmailEventAsync(Guid userId);

        /// <summary>
        /// 用户确认邮箱
        /// </summary>
        Task RequireToTrustedEmailAsync(Guid userId, ConfirmMailInputDto dto);

        /// <summary>
        /// 用户请求验证手机号
        /// </summary>
        Task PublishUserRequireToTrustPhoneNumEventAsync(Guid userId);
    }
}