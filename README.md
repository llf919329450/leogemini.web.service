﻿# LeoGee品牌商城开发文档

项目采用DDD思想进行分层架构设计，各项目文件描述如下：

基础业务层：

- Bitbits.LeoGemini.Application（应用层）
- Bitbits.LeoGemini.Domain（领域层）
- Bitbits.LeoGemini.Domain.Shared（公共领域层）
- Bitbits.LeoGemini.EntityFrameworkCore（数据库基础设施层）
- Bitbits.LeoGemini.Infras（基础设施层）

微服务层：

| 项目 | 服务名 | Http端口 |
| :--- | :--- | :---: |
| UserService | 用户服务 | 55051 |
| MailService | 邮件服务 | 55052 |
| GoodsService | 商品服务 | 55053 |
| FileService | 文件服务 | 55054 |
| OrderService | 订单服务 | 55055 |
| Gift&WishService | 心愿单礼品袋服务 | 55056 |

## 技术栈

需要安装的：

| 名称 | 端口号 | 用户名 | 密码 |
| :--- | :--- | :---: | :---: |
| Redis(缓存) | 6379 | 无 | 无 |
| PostgreSQL(数据库) | 5432 | LeoGemini | LeoGemini |
| RabbitMQ(消息队列) | 邮件服务 | admin | admin |

## 说明

一些关键配置可以在启动时通过指定环境变量的方式进行配置：

### 配置项

#### 服务发现

- SERVICE_DISCOVERY_HOST
    - 说明：配置服务注册地址

例如：`dotnet run --SERVICE_DISCOVERY_HOST "127.0.0.1"`

- SERVICE_DISCOVERY_PORT
    - 说明：配置服务注册端口号

例如：`dotnet run --SERVICE_DISCOVERY_PORT "55054"`

## 坑点

- AbpVNext的分布式事件总线支持RabbitMQ，框架对MQ的配置文档没有详细说明各配置项的意义，查看源码后发现ClientName是为了创建队列时使用的，不指定ClientName无法创建队列，MQ就收不到消息，订阅服务无法消费消息。
- AbpVNext的邮件发信模块：Volo.Abp.Emailing使用的是.net的SmtpClient，且框架内部对应的发件逻辑不对现代邮件服务提供商支持，因此更换MailKit包。
