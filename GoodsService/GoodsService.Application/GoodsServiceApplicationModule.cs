﻿using LeoGemini.Service.Common.Module;
using LeoGemini.Service.Infrastructure;
using GoodsService.Domain;
using GoodsService.EntityFrameworkCore;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace GoodsService.Application
{
    [DependsOn(
        typeof(GoodsServiceDomainModule),
        typeof(GoodsServiceEntityFrameworkCoreModule),
        typeof(AbpAutoMapperModule),
        typeof(LeoGeminiFreeRedisModule),
        typeof(LeoGeminiRabbitMqModule),
        typeof(LeoGeminiConsulModule),
        typeof(LeoGeminiStaticFileModule),
        typeof(LeoGeminiServiceInfrastructureModule))]
    public class GoodsServiceApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddProfile<GoodsServiceAutoMapperProfile>(validate: true);
            });
        }
    }
}