﻿using AutoMapper;
using GoodsService.Application.Contract.DTO.Output;
using GoodsService.Domain.Aggregate.Collection;
using GoodsService.Domain.Aggregate.Goods;

namespace GoodsService.Application
{
    public class GoodsServiceAutoMapperProfile : Profile
    {
        public GoodsServiceAutoMapperProfile()
        {
            CreateMap<Goods, GetGoodsReturnDto>();
            CreateMap<GoodsAttribute, GetGoodsAttributeReturnDto>();
            CreateMap<GoodsSku, GetGoodsSkuReturnDto>();
            CreateMap<GoodsSpec, GetGoodsSpecReturnDto>();

            CreateMap<Collection, CreateCollectionReturnDto>();
        }
    }
}