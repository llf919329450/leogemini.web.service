﻿using System.Threading.Tasks;
using GoodsService.Domain.Aggregate.Collection;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities.Events;
using Volo.Abp.EventBus;

namespace GoodsService.Application.EventHandler
{
    /// <summary>
    /// 系列创建完毕事件处理程序
    /// </summary>
    public class CollectionCreatedEventHandler : ILocalEventHandler<EntityCreatedEventData<Collection>>, ITransientDependency
    {
        /// <summary>
        /// 处理事件
        /// </summary>
        public Task HandleEventAsync(EntityCreatedEventData<Collection> eventData)
        {
            // TODO: 此处发布系列订阅推送逻辑
            throw new System.NotImplementedException();
        }
    }
}