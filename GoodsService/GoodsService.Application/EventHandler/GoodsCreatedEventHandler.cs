﻿using System.Threading.Tasks;
using GoodsService.Domain.Aggregate.Goods;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities.Events;
using Volo.Abp.EventBus;

namespace GoodsService.Application.EventHandler
{
    /// <summary>
    /// 商品创建完毕事件处理程序
    /// </summary>
    public class GoodsCreatedEventHandler : ILocalEventHandler<EntityCreatedEventData<Goods>>, ITransientDependency
    {
        /// <summary>
        /// 处理事件
        /// </summary>
        public Task HandleEventAsync(EntityCreatedEventData<Goods> eventData)
        {
            // TODO: 此处发布商品订阅推送逻辑
            throw new System.NotImplementedException();
        }
    }
}