﻿namespace GoodsService.Application
{
    public static class ErrorMessage
    {
        public const string PAGINATION_PEGE_ERROR = "输入页面参数不能为0";
        public const string GOODS_NOT_FOUND = "商品不存在";
        public const string COLLECTION_NOT_FOUND = "系列信息不存在";
    }
}