﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;

namespace GoodsService.Domain.Aggregate.Collection
{
    /// <summary>
    /// 商品系列聚合根
    /// </summary>
    [Table("tb_goods_collection")]
    public class Collection : AggregateRoot<string>, IHasCreationTime
    {
        /// <summary>
        /// ORM框架使用
        /// </summary>
        protected Collection() { }

        public Collection(string id)
        {
            Id = id;
            CreationTime = DateTime.Now;
        }

        public Collection(string id, string title, string intro) : this(id)
        {
            Title = title;
            Intro = intro;
        }
        
        /// <summary>
        /// 系列标题
        /// </summary>
        [Column("title")]
        public virtual string Title { get; private set; }
        
        /// <summary>
        /// 系列简介
        /// </summary>
        [Column("intro")]
        public virtual string Intro { get; private set; }
        
        public virtual DateTime CreationTime { get; set; }

        /// <summary>
        /// 修改标题
        /// </summary>
        public void ChangeTitle(string title)
        {
            Title = title;
        }

        /// <summary>
        /// 修改介绍
        /// </summary>
        public void ChangeIntro(string intro)
        {
            Intro = intro;
        }
    }
}