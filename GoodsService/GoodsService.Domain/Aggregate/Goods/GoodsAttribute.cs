﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Values;

namespace GoodsService.Domain.Aggregate.Goods
{
    /// <summary>
    /// 商品属性值对象
    /// </summary>
    [Table("tb_goods_attribute")]
    public class GoodsAttribute : ValueObject
    {
        /// <summary>
        /// ORM框架使用
        /// </summary>
        protected GoodsAttribute() { }
        
        public GoodsAttribute(string type, string title)
        {
            AttrType = type;
            AttrTitle = title;
        }
        
        /// <summary>
        /// 属性类型标识
        /// </summary>
        [Column("attr_type")]
        public virtual string AttrType { get; private set; }
        
        /// <summary>
        /// 属性名称
        /// </summary>
        [Column("attr_title")]
        public virtual string AttrTitle { get; private set; }
        
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return AttrType;
            yield return AttrTitle;
        }
    }
}