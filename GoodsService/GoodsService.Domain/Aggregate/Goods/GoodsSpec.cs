﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Values;

namespace GoodsService.Domain.Aggregate.Goods
{
    /// <summary>
    /// 商品属性实体
    /// </summary>
    [Table("tb_goods_sku_spec")]
    public class GoodsSpec : ValueObject
    {
        /// <summary>
        /// ORM框架使用
        /// </summary>
        protected GoodsSpec()
        {
            
        }

        internal GoodsSpec(string specType, string specValue)
        {
            SpecType = specType;
            SpecValue = specValue;
        }

        /// <summary>
        /// 属性类型标识
        /// </summary>
        [Column("spec_type")]
        public virtual string SpecType { get; internal set; }
        
        /// <summary>
        /// 属性值
        /// </summary>
        [Column("spec_value")]
        public virtual string SpecValue { get; internal set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return SpecType;
            yield return SpecValue;
        }
    }
}