﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using LeoGemini.Service.Infrastructure.Handler.Exception;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;

namespace GoodsService.Domain.Aggregate.Goods
{
    /// <summary>
    /// 商品聚合根
    /// </summary>
    [Table("tb_goods")]
    public class Goods : AggregateRoot<string>, IHasCreationTime
    {
        /// <summary>
        /// ORM框架使用
        /// </summary>
        protected Goods() { }

        public Goods(string id)
        {
            Id = id;
            Attributes = new List<GoodsAttribute>();
            Skus = new List<GoodsSku>();
            CreationTime = DateTime.Now;
        }

        /// <summary>
        /// 系列id
        /// </summary>
        [Column("collection_id")]
        public virtual string CollectionId { get; internal set; }
        
        /// <summary>
        /// 商品标题
        /// </summary>
        [Column("title")]
        public virtual string Title { get; internal set; }
        
        /// <summary>
        /// 商品类型
        /// </summary>
        [Column("type")]
        public virtual string Type { get; internal set; }
        
        /// <summary>
        /// 商品描述
        /// </summary>
        [Column("description")]
        public virtual string Description { get; internal set; }
        
        /// <summary>
        /// 商品详情
        /// </summary>
        [Column("details")]
        public virtual string[] Details { get; internal set; }
        
        /// <summary>
        /// 是否多规格
        /// </summary>
        [Column("has_multi_skus")]
        public virtual bool HasMultiSkus { get; internal set; }

        /// <summary>
        /// 商品预览图
        /// </summary>
        [Column("previews")]
        public virtual string[] PreViews { get; internal set; }
        
        /// <summary>
        /// 商品详情图
        /// </summary>
        [Column("shows")]
        public virtual string[] Shows { get; internal set; }
        
        /// <summary>
        /// 商品属性
        /// </summary>
        public virtual ICollection<GoodsAttribute> Attributes { get; private set; }

        /// <summary>
        /// 商品发布时间
        /// </summary>
        public virtual DateTime CreationTime { get; set; }
        
        /// <summary>
        /// 商品库存属性
        /// </summary>
        public virtual ICollection<GoodsSku> Skus { get; private set; }

        /// <summary>
        /// 添加商品属性标记
        /// </summary>
        public void DeclareAttribute(string type, string title)
        {
            if (Attributes.Any(attr => attr.AttrType == type && attr.AttrTitle == title))
                throw new ConflictException(ErrorMessage.GOODS_ATTRIBUTE_CONFLICT);
            Attributes.Add(new GoodsAttribute(type, title));
        }

        /// <summary>
        /// 添加库存单元
        /// </summary>
        public void AddSku(GoodsSku sku)
        {
            // 库存单元已存在
            if (Skus.Any(s => s.Id == sku.Id))
                throw new ConflictException(ErrorMessage.GOODS_SKU_CONFLICT);
            
            Skus.Add(sku);
            
            // 标记是否多库存单元
            if (Skus.Count > 1)
            {
                HasMultiSkus = true;
            }
        }

        /// <summary>
        /// 删除库存单元
        /// </summary>
        public void RemoveSku(Guid skuId)
        {
            // 查找库存单元
            var sku = Skus.First(s => s.Id == skuId);
            
            // 库存单元不存在
            if (sku == null)
                throw new NotFoundException(ErrorMessage.GOODS_SKU_NOTFOUND);
            
            Skus.Remove(sku);
            
            // 标记是否多库存单元
            if (Skus.Count <= 1)
            {
                HasMultiSkus = false;
            }
        }
    }
}