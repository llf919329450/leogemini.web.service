﻿namespace GoodsService.Domain
{
    public static class ErrorMessage
    {
        public const string GOODS_ATTRIBUTE_CONFLICT = "商品属性冲突";
        public const string GOODS_SKU_CONFLICT = "商品库存单元冲突";
        public const string GOODS_SKU_NOTFOUND = "商品库存单元未找到";
        public const string GOODS_SPEC_CONFLICT = "商品库存单元属性冲突";
    }
}