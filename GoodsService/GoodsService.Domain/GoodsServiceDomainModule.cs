﻿using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace GoodsService.Domain
{
    [DependsOn(typeof(AbpIdentityDomainModule))]
    public class GoodsServiceDomainModule : AbpModule
    {
    }
}