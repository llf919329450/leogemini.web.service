﻿using GoodsService.Domain.Aggregate.Goods;
using Volo.Abp.Domain.Repositories;

namespace GoodsService.Domain.Repository
{
    public interface IGoodsRepository : IRepository<Goods, string>
    {

    }
}