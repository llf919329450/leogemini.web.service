﻿using GoodsService.Domain.Aggregate.Collection;
using Volo.Abp.Domain.Repositories;

namespace GoodsService.Domain.Repository
{
    public interface ICollectionRepository : IRepository<Collection, string>
    {
        
    }
}