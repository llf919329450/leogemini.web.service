﻿using System;
using GoodsService.Domain.Aggregate.Goods;
using LeoGemini.Service.Infrastructure.Extension;
using Volo.Abp.DependencyInjection;

namespace GoodsService.Domain.Factory.Impl
{
    public class GoodsFactory : IGoodsFactory, ITransientDependency
    {
        public Goods Produce(string collectionId, string title, string type, string description, string[] details,
            string[] previews, string[] shows)
        {
            var goods = new Goods(new Random().Next(6, Extensions.MixinType.Upper))
            {
                CollectionId = collectionId,
                Title = title,
                Type = type,
                Description = description,
                Details = details,
                PreViews = previews,
                Shows = shows
            };

            return goods;
        }
    }
}