﻿using GoodsService.Domain.Aggregate.Goods;

namespace GoodsService.Domain.Factory
{
    public interface IGoodsFactory
    {
        Goods Produce(string collectionId, string title, string type, string description, string[] details, string[] previews, string[] shows);
    }
}