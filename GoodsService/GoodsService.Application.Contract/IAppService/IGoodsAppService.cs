﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GoodsService.Application.Contract.DTO.Input;
using GoodsService.Application.Contract.DTO.Output;

namespace GoodsService.Application.Contract.IAppService
{
    /// <summary>
    /// 商品服务规约
    /// </summary>
    public interface IGoodsAppService
    {
        /// <summary>
        /// 分页获取商品id
        /// </summary>
        Task<PaginationOutputDto<string>> GetGoodsIdsByPaginationAsync(int p, int n);
        
        /// <summary>
        /// 根据id获取商品
        /// </summary>
        Task<GetGoodsReturnDto> GetGoodsByIdAsync(string goodsId);

        /// <summary>
        /// 添加商品
        /// </summary>
        Task<GetGoodsReturnDto> CreateGoodsAsync(CreateGoodsInputDto dto);

        /// <summary>
        /// 删除商品
        /// </summary>
        Task DeleteGoodsByIdAsync(string goodsId);

        /// <summary>
        /// 修改商品
        /// </summary>
        Task<GetGoodsReturnDto> EditGoodsByIdAsync(string goodsId, EditGoodsInputDto dto);

        /// <summary>
        /// 根据系列id分页获取商品id
        /// </summary>
        Task<PaginationOutputDto<string>> GetGoodsIdsByCollectionIdAndPaginationAsync(string collectionId, int p, int n);
    }
}