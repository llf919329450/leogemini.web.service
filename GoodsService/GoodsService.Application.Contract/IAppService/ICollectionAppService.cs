﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GoodsService.Application.Contract.DTO.Input;
using GoodsService.Application.Contract.DTO.Output;

namespace GoodsService.Application.Contract.IAppService
{
    public interface ICollectionAppService
    {
        /// <summary>
        /// 分页获取系列id
        /// </summary>
        Task<PaginationOutputDto<string>> GetCollectionsByPaginationAsync(int p, int n);

        /// <summary>
        /// 根据系列id获取系列信息
        /// </summary>
        Task<CreateCollectionReturnDto> GetCollectionByIdAsync(string collectionId);

        /// <summary>
        /// 添加系列
        /// </summary>
        Task<CreateCollectionReturnDto> CreateCollectionAsync(CreateCollectionInputDto dto);

        /// <summary>
        /// 删除系列
        /// </summary>
        Task DeleteCollectionByIdAsync(string collectionId);

        /// <summary>
        /// 修改系列
        /// </summary>
        Task<CreateCollectionReturnDto> EditCollectionByIdAsync(string collectionId, EditCollectionInputDto dto);
    }
}