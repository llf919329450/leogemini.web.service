﻿namespace GoodsService.Application.Contract.DTO.Input
{
    public class EditCollectionInputDto
    {
        /// <summary>
        /// 系列id
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// 系列标题
        /// </summary>
        public string Title { get;  set; }
        
        /// <summary>
        /// 系列简介
        /// </summary>
        public string Intro { get;  set; }
    }
}