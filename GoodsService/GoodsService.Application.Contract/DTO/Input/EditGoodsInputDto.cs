﻿using System.Collections.Generic;

namespace GoodsService.Application.Contract.DTO.Input
{
    public class EditGoodsInputDto
    {
        /// <summary>
        /// 商品id
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// 系列id
        /// </summary>
        public string CollectionId { get; set; }
        
        /// <summary>
        /// 商品标题
        /// </summary>
        public string Title { get; set; }
        
        /// <summary>
        /// 商品类型
        /// </summary>
        public string Type { get; set; }
        
        /// <summary>
        /// 商品描述
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// 商品详情
        /// </summary>
        public string[] Details { get; set; }

        /// <summary>
        /// 商品预览图
        /// </summary>
        public string[] PreViews { get; set; }
        
        /// <summary>
        /// 商品详情图
        /// </summary>
        public string[] Shows { get; set; }
        
        /// <summary>
        /// 商品属性
        /// </summary>
        public ICollection<EditGoodsAttributeInputDto> Attributes { get; set; }
        
        /// <summary>
        /// 商品库存单元
        /// </summary>
        public ICollection<EditGoodsSkuInputDto> Skus { get; set; }
    }
    
    public class EditGoodsAttributeInputDto
    {
        /// <summary>
        /// 属性类型标识
        /// </summary>
        public string AttrType { get; set; }
        
        /// <summary>
        /// 属性名称
        /// </summary>
        public string AttrTitle { get; set; }
    }

    public class EditGoodsSkuInputDto
    {
        /// <summary>
        /// 现价
        /// </summary>
        public decimal Price { get; set; }
        
        /// <summary>
        /// 库存数量
        /// </summary>
        public int Stock { get; set; }
        
        /// <summary>
        /// 库存单位
        /// </summary>
        public string Unit { get; set; }
        
        /// <summary>
        /// 是否显示库存
        /// </summary>
        public bool IsShowStock { get; set; }
        
        /// <summary>
        /// Sku属性
        /// </summary>
        public ICollection<EditGoodsSpecInputDto> Specs { get; set; }
    }

    public class EditGoodsSpecInputDto
    {
        /// <summary>
        /// 属性类型标识
        /// </summary>
        public string SpecType { get; set; }
        
        /// <summary>
        /// 属性值
        /// </summary>
        public string SpecValue { get; set; }
    }
}