﻿using System.Collections.Generic;

namespace GoodsService.Application.Contract.DTO.Output
{
    /// <summary>
    /// 分页dto
    /// </summary>
    public class PaginationOutputDto<TEntityId>
    {
        /// <summary>
        /// 记录总数
        /// </summary>
        public int Num { get; set; }
        
        /// <summary>
        /// 分页id
        /// </summary>
        public ICollection<TEntityId> Ids { get; set; }
    }
}