﻿using System;
using System.Collections.Generic;

namespace GoodsService.Application.Contract.DTO.Output
{
    public class GetGoodsReturnDto
    {
        /// <summary>
        /// 商品id
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// 系列id
        /// </summary>
        public string CollectionId { get; set; }
        
        /// <summary>
        /// 商品标题
        /// </summary>
        public string Title { get; set; }
        
        /// <summary>
        /// 商品类型
        /// </summary>
        public string Type { get; set; }
        
        /// <summary>
        /// 商品描述
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// 商品详情
        /// </summary>
        public string[] Details { get; set; }
        
        /// <summary>
        /// 是否多规格
        /// </summary>
        public bool HasMultiSkus { get; set; }

        /// <summary>
        /// 商品预览图
        /// </summary>
        public string[] PreViews { get; set; }
        
        /// <summary>
        /// 商品详情图
        /// </summary>
        public string[] Shows { get; set; }
        
        /// <summary>
        /// 商品属性
        /// </summary>
        public ICollection<GetGoodsAttributeReturnDto> Attributes { get; set; }
        
        /// <summary>
        /// 商品发布时间
        /// </summary>
        public DateTime CreationTime { get; set; }
        
        /// <summary>
        /// 库存单元
        /// </summary>
        public ICollection<GetGoodsSkuReturnDto> Skus { get; set; }
    }

    public class GetGoodsAttributeReturnDto
    {
        /// <summary>
        /// 属性类型标识
        /// </summary>
        public string AttrType { get; set; }
        
        /// <summary>
        /// 属性名称
        /// </summary>
        public string AttrTitle { get; set; }
    }

    public class GetGoodsSkuReturnDto
    {
        /// <summary>
        /// 现价
        /// </summary>
        public decimal Price { get; set; }
        
        /// <summary>
        /// 原价
        /// </summary>
        public decimal CostPrice { get; set; }
        
        /// <summary>
        /// 库存数量
        /// </summary>
        public int Stock { get; set; }
        
        /// <summary>
        /// 库存单位
        /// </summary>
        public virtual string Unit { get; set; }
        
        /// <summary>
        /// 是否显示库存
        /// </summary>
        public virtual string IsShowStock { get; set; }
        
        /// <summary>
        /// Sku属性
        /// </summary>
        public virtual ICollection<GetGoodsSpecReturnDto> Specs { get; set; }
    }

    public class GetGoodsSpecReturnDto
    {
        /// <summary>
        /// 属性类型标识
        /// </summary>
        public string SpecType { get; set; }
        
        /// <summary>
        /// 属性值
        /// </summary>
        public string SpecValue { get; set; }
    }
}