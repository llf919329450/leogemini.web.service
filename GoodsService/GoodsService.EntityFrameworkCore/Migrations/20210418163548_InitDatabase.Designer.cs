﻿// <auto-generated />
using System;
using GoodsService.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using Volo.Abp.EntityFrameworkCore;

namespace GoodsService.EntityFrameworkCore.Migrations
{
    [DbContext(typeof(GoodsServiceDbContext))]
    [Migration("20210418163548_InitDatabase")]
    partial class InitDatabase
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("_Abp_DatabaseProvider", EfCoreDatabaseProvider.PostgreSql)
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.5")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("GoodsService.Domain.Aggregate.Collection.Collection", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasMaxLength(40)
                        .HasColumnType("character varying(40)")
                        .HasColumnName("ConcurrencyStamp");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CreationTime");

                    b.Property<string>("ExtraProperties")
                        .HasColumnType("text")
                        .HasColumnName("ExtraProperties");

                    b.Property<string>("Intro")
                        .HasColumnType("text")
                        .HasColumnName("intro");

                    b.Property<string>("Title")
                        .HasColumnType("text")
                        .HasColumnName("title");

                    b.HasKey("Id");

                    b.ToTable("tb_goods_collection");
                });

            modelBuilder.Entity("GoodsService.Domain.Aggregate.Goods.Goods", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text");

                    b.Property<string>("CollectionId")
                        .HasColumnType("text")
                        .HasColumnName("collection_id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasMaxLength(40)
                        .HasColumnType("character varying(40)")
                        .HasColumnName("ConcurrencyStamp");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CreationTime");

                    b.Property<string>("Description")
                        .HasColumnType("text")
                        .HasColumnName("description");

                    b.Property<string[]>("Details")
                        .HasColumnType("text[]")
                        .HasColumnName("details");

                    b.Property<string>("ExtraProperties")
                        .HasColumnType("text")
                        .HasColumnName("ExtraProperties");

                    b.Property<bool>("HasMultiSkus")
                        .HasColumnType("boolean")
                        .HasColumnName("has_multi_skus");

                    b.Property<string[]>("PreViews")
                        .HasColumnType("text[]")
                        .HasColumnName("previews");

                    b.Property<string[]>("Shows")
                        .HasColumnType("text[]")
                        .HasColumnName("shows");

                    b.Property<string>("Title")
                        .HasColumnType("text")
                        .HasColumnName("title");

                    b.Property<string>("Type")
                        .HasColumnType("text")
                        .HasColumnName("type");

                    b.HasKey("Id");

                    b.ToTable("tb_goods");
                });

            modelBuilder.Entity("GoodsService.Domain.Aggregate.Goods.GoodsSku", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("uuid");

                    b.Property<decimal>("CostPrice")
                        .HasColumnType("numeric")
                        .HasColumnName("cost_price");

                    b.Property<DateTime>("CreationTime")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CreationTime");

                    b.Property<bool>("IsShowStock")
                        .HasColumnType("boolean")
                        .HasColumnName("is_show_stock");

                    b.Property<decimal>("Price")
                        .HasColumnType("numeric")
                        .HasColumnName("price");

                    b.Property<string>("ReferenceGoodsId")
                        .HasColumnType("text")
                        .HasColumnName("reference_goods_id");

                    b.Property<int>("Stock")
                        .HasColumnType("integer")
                        .HasColumnName("stock");

                    b.Property<string>("Unit")
                        .HasColumnType("text")
                        .HasColumnName("unit");

                    b.HasKey("Id");

                    b.HasIndex("ReferenceGoodsId");

                    b.ToTable("tb_goods_sku");
                });

            modelBuilder.Entity("GoodsService.Domain.Aggregate.Goods.Goods", b =>
                {
                    b.OwnsMany("GoodsService.Domain.Aggregate.Goods.GoodsAttribute", "Attributes", b1 =>
                        {
                            b1.Property<string>("GoodsId")
                                .HasColumnType("text");

                            b1.Property<int>("Id")
                                .ValueGeneratedOnAdd()
                                .HasColumnType("integer")
                                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                            b1.Property<string>("AttrTitle")
                                .HasColumnType("text")
                                .HasColumnName("attr_title");

                            b1.Property<string>("AttrType")
                                .HasColumnType("text")
                                .HasColumnName("attr_type");

                            b1.HasKey("GoodsId", "Id");

                            b1.ToTable("tb_goods_attribute");

                            b1.WithOwner()
                                .HasForeignKey("GoodsId");
                        });

                    b.Navigation("Attributes");
                });

            modelBuilder.Entity("GoodsService.Domain.Aggregate.Goods.GoodsSku", b =>
                {
                    b.HasOne("GoodsService.Domain.Aggregate.Goods.Goods", "Goods")
                        .WithMany("Skus")
                        .HasForeignKey("ReferenceGoodsId");

                    b.OwnsMany("GoodsService.Domain.Aggregate.Goods.GoodsSpec", "Specs", b1 =>
                        {
                            b1.Property<Guid>("GoodsSkuId")
                                .HasColumnType("uuid");

                            b1.Property<int>("Id")
                                .ValueGeneratedOnAdd()
                                .HasColumnType("integer")
                                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                            b1.Property<string>("SpecType")
                                .HasColumnType("text")
                                .HasColumnName("spec_type");

                            b1.Property<string>("SpecValue")
                                .HasColumnType("text")
                                .HasColumnName("spec_value");

                            b1.HasKey("GoodsSkuId", "Id");

                            b1.ToTable("tb_goods_sku_spec");

                            b1.WithOwner()
                                .HasForeignKey("GoodsSkuId");
                        });

                    b.Navigation("Goods");

                    b.Navigation("Specs");
                });

            modelBuilder.Entity("GoodsService.Domain.Aggregate.Goods.Goods", b =>
                {
                    b.Navigation("Skus");
                });
#pragma warning restore 612, 618
        }
    }
}
