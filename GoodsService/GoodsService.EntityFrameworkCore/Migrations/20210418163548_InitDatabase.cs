﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace GoodsService.EntityFrameworkCore.Migrations
{
    public partial class InitDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tb_goods",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    collection_id = table.Column<string>(type: "text", nullable: true),
                    title = table.Column<string>(type: "text", nullable: true),
                    type = table.Column<string>(type: "text", nullable: true),
                    description = table.Column<string>(type: "text", nullable: true),
                    details = table.Column<string[]>(type: "text[]", nullable: true),
                    has_multi_skus = table.Column<bool>(type: "boolean", nullable: false),
                    previews = table.Column<string[]>(type: "text[]", nullable: true),
                    shows = table.Column<string[]>(type: "text[]", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ExtraProperties = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_goods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tb_goods_collection",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    title = table.Column<string>(type: "text", nullable: true),
                    intro = table.Column<string>(type: "text", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ExtraProperties = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_goods_collection", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tb_goods_attribute",
                columns: table => new
                {
                    GoodsId = table.Column<string>(type: "text", nullable: false),
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    attr_type = table.Column<string>(type: "text", nullable: true),
                    attr_title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_goods_attribute", x => new { x.GoodsId, x.Id });
                    table.ForeignKey(
                        name: "FK_tb_goods_attribute_tb_goods_GoodsId",
                        column: x => x.GoodsId,
                        principalTable: "tb_goods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tb_goods_sku",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    reference_goods_id = table.Column<string>(type: "text", nullable: true),
                    price = table.Column<decimal>(type: "numeric", nullable: false),
                    cost_price = table.Column<decimal>(type: "numeric", nullable: false),
                    stock = table.Column<int>(type: "integer", nullable: false),
                    unit = table.Column<string>(type: "text", nullable: true),
                    is_show_stock = table.Column<bool>(type: "boolean", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_goods_sku", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tb_goods_sku_tb_goods_reference_goods_id",
                        column: x => x.reference_goods_id,
                        principalTable: "tb_goods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tb_goods_sku_spec",
                columns: table => new
                {
                    GoodsSkuId = table.Column<Guid>(type: "uuid", nullable: false),
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    spec_type = table.Column<string>(type: "text", nullable: true),
                    spec_value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_goods_sku_spec", x => new { x.GoodsSkuId, x.Id });
                    table.ForeignKey(
                        name: "FK_tb_goods_sku_spec_tb_goods_sku_GoodsSkuId",
                        column: x => x.GoodsSkuId,
                        principalTable: "tb_goods_sku",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tb_goods_sku_reference_goods_id",
                table: "tb_goods_sku",
                column: "reference_goods_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tb_goods_attribute");

            migrationBuilder.DropTable(
                name: "tb_goods_collection");

            migrationBuilder.DropTable(
                name: "tb_goods_sku_spec");

            migrationBuilder.DropTable(
                name: "tb_goods_sku");

            migrationBuilder.DropTable(
                name: "tb_goods");
        }
    }
}
