﻿using GoodsService.Domain.Aggregate.Goods;
using GoodsService.Domain.Repository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace GoodsService.EntityFrameworkCore.Repository
{
    public class GoodsRepository : EfCoreRepository<GoodsServiceDbContext, Goods, string>, IGoodsRepository
    {
        public GoodsRepository(IDbContextProvider<GoodsServiceDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}