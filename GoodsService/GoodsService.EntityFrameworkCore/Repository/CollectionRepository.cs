﻿using GoodsService.Domain.Aggregate.Collection;
using GoodsService.Domain.Repository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace GoodsService.EntityFrameworkCore.Repository
{
    public class CollectionRepository : EfCoreRepository<GoodsServiceDbContext, Collection, string>, ICollectionRepository
    {
        public CollectionRepository(IDbContextProvider<GoodsServiceDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}