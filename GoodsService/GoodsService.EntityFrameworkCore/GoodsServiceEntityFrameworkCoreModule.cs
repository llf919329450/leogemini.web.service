﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using GoodsService.Domain;
using GoodsService.Domain.Aggregate.Goods;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.PostgreSql;
using Volo.Abp.Modularity;

namespace GoodsService.EntityFrameworkCore
{
    [DependsOn(
        typeof(GoodsServiceDomainModule),
        typeof(AbpEntityFrameworkCoreModule),
        typeof(AbpEntityFrameworkCorePostgreSqlModule))]
    public class GoodsServiceEntityFrameworkCoreModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<GoodsServiceDbContext>(options =>
            {
                options.AddDefaultRepositories(true);
                options.Entity<Goods>(g => 
                    g.DefaultWithDetailsFunc = goods => goods.Include(n => n.Skus) );
            });

            // 配置复用连接
            Configure<AbpDbContextOptions>(options =>
            {
                options.Configure(ctx =>
                {
                    if (ctx.ExistingConnection != null)
                    {
                        ctx.DbContextOptions.UseNpgsql(ctx.ExistingConnection);
                    }
                    else
                    {
                        ctx.DbContextOptions.UseNpgsql(ctx.ConnectionString);
                    }
                });
            });
        }
    }
}