﻿using LeoGemini.Service.Infrastructure.Constract;
using Microsoft.AspNetCore.Mvc;

namespace GoodsService.HttpApi.Hosting.Controller
{
    [Route("health")]
    public class HealthController : ApiResponseController
    {
        [HttpGet("check")]
        public void Check()
        {
            
        }
    }
}