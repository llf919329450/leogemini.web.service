﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GoodsService.Application.Contract.DTO.Input;
using GoodsService.Application.Contract.DTO.Output;
using GoodsService.Application.Contract.IAppService;
using LeoGemini.Service.Infrastructure.Constract;
using LeoGemini.Service.Infrastructure.Extension;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace GoodsService.HttpApi.Hosting.Controller
{
    [Route("api/v1/goods")]
    public class GoodsController : ApiResponseController
    {
        private readonly IGoodsAppService _goodsAppService;
        private readonly IWebHostEnvironment _env;
        
        public GoodsController(
            IWebHostEnvironment env,
            IGoodsAppService goodsAppService)
        {
            _goodsAppService = goodsAppService;
            _env = env;
        }

        /// <summary>
        /// 分页获取商品id
        /// </summary>
        [HttpGet]
        public async Task<object> GetGoodsByPagination(int p, int n = 10)
        {
            return await _goodsAppService.GetGoodsIdsByPaginationAsync(p - 1, n);
        }

        /// <summary>
        /// 根据id获取商品
        /// </summary>
        [HttpGet("{goodsId}/details")]
        public async Task<object> GetGoodsById(string goodsId)
        {
            return await _goodsAppService.GetGoodsByIdAsync(goodsId);
        }

        /// <summary>
        /// 添加商品
        /// </summary>
        [HttpPost("create")]
        public async Task<object> CreateGoods(CreateGoodsInputDto dto)
        {
            return await _goodsAppService.CreateGoodsAsync(dto);
        }

        /// <summary>
        /// 删除商品
        /// </summary>
        [HttpDelete("{goodsId}/delete")]
        public async Task DeleteGoodsById(string goodsId)
        {
            await _goodsAppService.DeleteGoodsByIdAsync(goodsId);
        }

        /// <summary>
        /// 修改商品
        /// </summary>
        [HttpPut("{goodsId}/edit")]
        public async Task<object> EditGoodsById(string goodsId, EditGoodsInputDto dto)
        {
            return await _goodsAppService.EditGoodsByIdAsync(goodsId, dto);
        }

        /// <summary>
        /// 导入商品信息excel
        /// </summary>
        /// <returns></returns>
        [HttpPost("imports")]
        public async Task<object> ImportGoodsExcel(IFormFile file)
        {
            // 写出文件
            var path = file.SaveTo(Path.Combine(_env.WebRootPath, "goods")).Replace('/', '\\');
            return await ExportGoodsFromExcel(path);
        }
        
        private async Task<ICollection<GetGoodsReturnDto>> ExportGoodsFromExcel(string filePath)
        {
            FileInfo file = new FileInfo(filePath);
            using (ExcelPackage package = new ExcelPackage(file))
            {
                var collections = new List<GetGoodsReturnDto>();
                ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                //获取表格的列数和行数
                int rowCount = worksheet.Dimension.Rows;
                for (int row = 2; row <= rowCount; row++)
                {
                    var collection = await _goodsAppService.CreateGoodsAsync(new CreateGoodsInputDto
                    {

                    });
                    collections.Add(collection);
                }

                return collections;
            }
        }
    }
}