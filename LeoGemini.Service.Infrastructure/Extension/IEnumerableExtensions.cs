﻿using System;
using System.Collections.Generic;

namespace LeoGemini.Service.Infrastructure.Extension
{
    public static partial class Extensions
    {
        public static List<TResult> Map<T, TResult>(this IEnumerable<T> list, Func<T, TResult> action)
        {
            List<TResult> _list = new List<TResult>();
            foreach (T data in list)
            {
                _list.Add(action(data));
            }
            return _list;
        }
        
        public static void ForEach<T>(this IEnumerable<T> arr, Action<T> action)
        {
            foreach (T data in arr)
            {
                action(data);
            }
        }

        public static void ForEach<T>(this IEnumerable<T> arr, Action<int, T> action)
        {
            int i = 0;
            foreach (T data in arr)
            {
                action(i++, data);
            }
        }
        
        public static T[] Filter<T>(this IEnumerable<T> arr, Func<T, bool> action)
        {
            List<T> _list = new List<T>();
            foreach (T data in arr)
            {
                if (action(data))
                {
                    _list.Add(data);
                }
            }
            return _list.ToArray();
        }
    }
}