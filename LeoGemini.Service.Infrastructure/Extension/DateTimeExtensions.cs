﻿using System;

namespace LeoGemini.Service.Infrastructure.Extension
{
    public static partial class Extensions
    {
        public static int ToUnixTimeTicks(this DateTime time)
        {
            DateTime start = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (int) (time - start).TotalMilliseconds;
        }
    }
}