﻿using System;
using System.Text;

namespace LeoGemini.Service.Infrastructure.Extension
{
    public static partial class Extensions
    {
        private static readonly string strSeed = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz1234567890";

        public enum MixinType
        {
            Mixin, Upper, Lower, JustNum
        }
        
        public static string Next(this Random random, int len, MixinType type)
        {
            var sb = new StringBuilder();
            var minPos = type == MixinType.JustNum ? strSeed.Length - 10 : 0;
            for (var i = 0; i < len; i++)
            {
                sb.Append(strSeed[random.Next(minPos, strSeed.Length)]);
            }

            return type switch
            {
                MixinType.Mixin => sb.ToString(),
                MixinType.Upper => sb.ToString().ToUpper(),
                MixinType.Lower => sb.ToString().ToLower(),
                _ => sb.ToString()
            };
        }
    }
}