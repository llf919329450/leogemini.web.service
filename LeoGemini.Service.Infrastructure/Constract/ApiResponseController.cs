﻿using LeoGemini.Service.Infrastructure.Handler.Filter;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace LeoGemini.Service.Infrastructure.Constract
{
    [ApiController]
    [ServiceFilter(typeof(ApiResponseFilter))]
    [ServiceFilter(typeof(HttpExceptionFilter))]
    public abstract class ApiResponseController : AbpController
    {
        
    }
}