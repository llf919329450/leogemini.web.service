﻿using System;
using System.Linq.Expressions;

namespace LeoGemini.Service.Infrastructure.Constract
{
    public interface IMapperSpecification<TSource, TDto>
    {
        Expression<Func<TSource, TDto>> ToExpression();
    }
}