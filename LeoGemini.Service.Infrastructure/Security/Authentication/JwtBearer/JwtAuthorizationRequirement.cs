﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;

namespace LeoGemini.Service.Infrastructure.Security.Authentication.JwtBearer
{
    public class JwtAuthorizationRequirement : IAuthorizationRequirement
    {
        public Func<HttpContext, bool> ValidatePermission { get; internal set; }

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public SigningCredentials SigningCredentials { get; set; }
    }
}