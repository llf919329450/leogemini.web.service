﻿using System;
using System.Security.Claims;

namespace LeoGemini.Service.Infrastructure.Security.Authentication.JwtBearer
{
    public interface ITokenBuilder
    {
        TokenBuilder.Token BuildJwtToken(Claim[] claims, DateTime? expires = null);

        TokenBuilder.Token BuildJwtToken(
            Claim[] claims,
            DateTime notBefore,
            DateTime? expires = null);

        TokenBuilder.Token BuildJwtToken(
            Claim[] claims,
            string ip,
            DateTime? notBefore = null,
            DateTime? expires = null);
    }
}