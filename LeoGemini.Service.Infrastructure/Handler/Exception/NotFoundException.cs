﻿namespace LeoGemini.Service.Infrastructure.Handler.Exception
{
    public class NotFoundException : HttpException
    {
        public NotFoundException(string message = "请求资源未找到") : base(404, message)
        {
        }
    }
}