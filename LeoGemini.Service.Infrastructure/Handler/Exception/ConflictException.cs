﻿namespace LeoGemini.Service.Infrastructure.Handler.Exception
{
    public class ConflictException : HttpException
    {
        public ConflictException(string message = "资源已存在") : base(409, message)
        {
        }
    }
}