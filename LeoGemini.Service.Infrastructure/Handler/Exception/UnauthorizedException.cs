﻿namespace LeoGemini.Service.Infrastructure.Handler.Exception
{
    public class UnauthorizedException : HttpException
    {
        public UnauthorizedException(string message = "请求未经授权") : base(401, message)
        {
        }
    }
}