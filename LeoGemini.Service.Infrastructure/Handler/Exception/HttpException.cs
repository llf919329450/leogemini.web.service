﻿using System;

namespace LeoGemini.Service.Infrastructure.Handler.Exception
{
    public class HttpException : System.Exception
    {
        public int HttpStatusCode { get; protected set; }
        
        public new string Message { get; protected set; }
        
        public new Object Data { get; set; }

        protected HttpException(int httpStatusCode, string message = "请求发生错误")
        {
            HttpStatusCode = httpStatusCode;
            Message = message;
        }
    }
}