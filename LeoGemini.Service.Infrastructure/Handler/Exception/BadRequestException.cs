﻿namespace LeoGemini.Service.Infrastructure.Handler.Exception
{
    public class BadRequestException : HttpException
    {
        public BadRequestException(string message = "请求发生错误") : base(400, message)
        {
        }
    }
}