﻿using LeoGemini.Service.Infrastructure.Handler.Filter;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace LeoGemini.Service.Infrastructure
{
    public class LeoGeminiServiceInfrastructureModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var services = context.Services;
            services.AddScoped<ApiResponseFilter>();
            services.AddScoped<HttpExceptionFilter>();
        }
    }
}