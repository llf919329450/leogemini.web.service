﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MailService.Application.Contract.DTO.Output;

namespace MailService.Application.Contract.IAppService
{
    public interface IMailAppService
    {
        /// <summary>
        /// 分页获取邮件id
        /// </summary>
        Task<ICollection<string>> GetMailByPaginationAsync(int p, int n);
        
        /// <summary>
        /// 邮件id获取邮件信息
        /// </summary>
        Task<GetMailReturnDto> GetMailByIdAsync(Guid id);

        /// <summary>
        /// 邮件id删除邮件
        /// </summary>
        Task DeleteMailByIdAsync(Guid id);
    }
}