﻿using System;
using MailService.Domain.Shared.Enum;

namespace MailService.Application.Contract.DTO.Output
{
    public class GetMailReturnDto
    {
        /// <summary>
        /// 邮件id
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 接收者
        /// </summary>
        public string Receiver { get; set; }
        
        /// <summary>
        /// 主题
        /// </summary>
        public string Subject { get; set; }
        
        /// <summary>
        /// 发送状态
        /// </summary>
        public MailState State { get; set; }
        
        /// <summary>
        /// 失败原因
        /// </summary>
        public string Reason { get; set; }
        
        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime CreationTime { get; set; }
    }
}