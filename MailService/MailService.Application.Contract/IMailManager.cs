﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MailService.Application.Contract
{
    public interface IMailManager
    {
        Task<Exception> SendMailWithTemplateAsync(string template, string receiver, string subject, Func<object> model,
            Func<Dictionary<string, object>> globalData = null);
    }
}