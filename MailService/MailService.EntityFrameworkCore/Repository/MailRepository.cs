﻿using System;
using MailService.Domain.Aggregate.Mail;
using MailService.Domain.Repository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace MailService.EntityFrameworkCore.Repository
{
    public class MailRepository : EfCoreRepository<MailServiceDbContext, Mail, Guid>, IMailRepository
    {
        public MailRepository(IDbContextProvider<MailServiceDbContext> dbContextProvider) : base(dbContextProvider) { }
    }
}