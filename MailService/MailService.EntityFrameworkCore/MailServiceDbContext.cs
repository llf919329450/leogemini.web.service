﻿using System.IO;
using MailService.Domain.Aggregate.Mail;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace MailService.EntityFrameworkCore
{
    /// <summary>
    /// 用户服务数据库上下文
    /// </summary>
    public class MailServiceDbContext : AbpDbContext<MailServiceDbContext>
    {
        public DbSet<Mail> Mail { get; set; }
        
        public MailServiceDbContext(DbContextOptions<MailServiceDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            // 配置数据库实体
            modelBuilder.Entity<Mail>(m =>
            {
                m.ConfigureByConvention();
            });
        }
    }

    public class LeoGeminiDbContextFactory : IDesignTimeDbContextFactory<MailServiceDbContext>
    {
        public MailServiceDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<MailServiceDbContext>()
                .UseNpgsql(configuration.GetConnectionString("Default"));

            return new MailServiceDbContext(builder.Options);
        }

        public static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);
            return builder.Build();
        }
    }
}