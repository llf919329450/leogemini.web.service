﻿using System;
using System.Threading.Tasks;
using LeoGemini.Service.Infrastructure.Constract;
using MailService.Application.Contract.IAppService;
using Microsoft.AspNetCore.Mvc;

namespace MailService.HttpApi.Hosting.Controller
{
    [Route("api/v1/mails")]
    public class MailsController : ApiResponseController
    {
        private readonly IMailAppService _mailAppService;

        public MailsController(IMailAppService mailAppService)
        {
            _mailAppService = mailAppService;
        }

        /// <summary>
        /// 分页获取邮件id
        /// </summary>
        [HttpGet]
        public async Task<object> GetMailByPagination(int p, int n = 10)
        {
            return await _mailAppService.GetMailByPaginationAsync(p - 1, n);
        }

        /// <summary>
        /// 邮件id获取邮件信息
        /// </summary>
        [HttpGet("{mailId}/details")]
        public async Task<object> GetMailById(Guid mailId)
        {
            return await _mailAppService.GetMailByIdAsync(mailId);
        }

        /// <summary>
        /// 邮件id删除邮件
        /// </summary>
        [HttpDelete("{mailId}/delete")]
        public async Task DeleteMailById(Guid mailId)
        {
            await _mailAppService.DeleteMailByIdAsync(mailId);
        }
    }
}