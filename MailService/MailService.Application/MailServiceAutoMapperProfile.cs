﻿using AutoMapper;
using MailService.Application.Contract.DTO.Output;
using MailService.Domain.Aggregate.Mail;

namespace MailService.Application
{
    public class MailServiceAutoMapperProfile : Profile
    {
        public MailServiceAutoMapperProfile()
        {
            CreateMap<Mail, GetMailReturnDto>();
        }
    }
}