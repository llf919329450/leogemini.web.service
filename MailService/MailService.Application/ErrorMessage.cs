﻿namespace MailService.Application
{
    public static class ErrorMessage
    {
        public const string PAGINATION_PEGE_ERROR = "输入页面参数不能为0";
        public const string MAIL_NOT_FOUND = "邮件不存在";
        
    }
}