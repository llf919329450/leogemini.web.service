﻿using LeoGemini.Service.Common.Module;
using LeoGemini.Service.Infrastructure;
using MailService.Domain;
using MailService.EntityFrameworkCore;
using Volo.Abp.AutoMapper;
using Volo.Abp.MailKit;
using Volo.Abp.Modularity;
using Volo.Abp.TextTemplating;
using Volo.Abp.VirtualFileSystem;

namespace MailService.Application
{
    [DependsOn(
        typeof(MailServiceDomainModule),
        typeof(MailServiceEntityFrameworkCoreModule),
        typeof(AbpAutoMapperModule),
        typeof(AbpMailKitModule),
        typeof(AbpTextTemplatingModule),
        typeof(AbpVirtualFileSystemModule),
        typeof(LeoGeminiRabbitMqModule),
        typeof(LeoGeminiConsulModule),
        typeof(LeoGeminiFreeRedisModule),
        typeof(LeoGeminiServiceInfrastructureModule))]
    public class MailServiceApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddProfile<MailServiceAutoMapperProfile>(validate: true);
            });
            
            Configure<AbpVirtualFileSystemOptions>(options =>
            {
                options.FileSets.AddEmbedded<MailServiceApplicationModule>(
                    baseNamespace: "MailService.Application",
                    baseFolder: "/Resource");
            });
        }
    }
}