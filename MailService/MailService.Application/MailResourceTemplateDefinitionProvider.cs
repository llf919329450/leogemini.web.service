﻿using Volo.Abp.TextTemplating;

namespace MailService.Application
{
    /// <summary>
    /// 邮件模板文件定义类
    /// </summary>
    public class MailResourceTemplateDefinitionProvider : TemplateDefinitionProvider
    {
        public static string MAIL_TEMPLATE_CONFIRM_MAIL = "ConfirmMail"; 
        public static string MAIL_TEMPLATE_CHECK_ORDER_MAIL = "CheckOrderMail"; 
            
        public override void Define(ITemplateDefinitionContext context)
        {
            // 添加模板定义
            context.Add(
                new TemplateDefinition(MAIL_TEMPLATE_CONFIRM_MAIL)
                    .WithVirtualFilePath("/Resource/Templates/ConfirmMailMail.tpl", isInlineLocalized: true),
                new TemplateDefinition(MAIL_TEMPLATE_CHECK_ORDER_MAIL)
                    .WithVirtualFilePath("/Resource/Templates/OrderCheckMail.tpl", isInlineLocalized: true)
            );
        }
    }
}