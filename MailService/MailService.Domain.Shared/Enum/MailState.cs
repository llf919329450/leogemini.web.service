﻿namespace MailService.Domain.Shared.Enum
{
    public enum MailState
    {
        Sending, Failed, Success
    }
}