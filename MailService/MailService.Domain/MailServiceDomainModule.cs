﻿using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace MailService.Domain
{
    [DependsOn(typeof(AbpIdentityDomainModule))]
    public class MailServiceDomainModule : AbpModule
    {
    }
}