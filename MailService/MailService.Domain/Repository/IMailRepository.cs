﻿using System;
using MailService.Domain.Aggregate.Mail;
using Volo.Abp.Domain.Repositories;

namespace MailService.Domain.Repository
{
    /// <summary>
    /// 邮件仓储接口
    /// </summary>
    public interface IMailRepository : IRepository<Mail, Guid>
    {
        
    }
}