﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using MailService.Domain.Shared.Enum;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;

namespace MailService.Domain.Aggregate.Mail
{
    /// <summary>
    /// 邮件聚合根
    /// </summary>
    [Table("tb_mail")]
    public class Mail : AggregateRoot<Guid>, IHasCreationTime
    {
        
        /// <summary>
        /// ORM框架使用
        /// </summary>
        protected Mail() { }

        public Mail(Guid id)
        {
            Id = id;
        }

        public Mail(Guid id, Guid userId, string receiver, string subject)
        {
            Id = id;
            UserId = userId;
            Receiver = receiver;
            Subject = subject;
            State = MailState.Sending;
        }
        
        /// <summary>
        /// 用户id
        /// </summary>
        [Column("user_id")]
        public virtual Guid UserId { get; private set; }

        /// <summary>
        /// 接收者
        /// </summary>
        [Column("receiver")]
        public virtual string Receiver { get; private set; }
        
        /// <summary>
        /// 主题
        /// </summary>
        [Column("subject")]
        public virtual string Subject { get; private set; }
        
        /// <summary>
        /// 发送状态
        /// </summary>
        [Column("state")]
        public virtual MailState State { get; private set; }
        
        /// <summary>
        /// 失败原因
        /// </summary>
        [Column("reason")]
        public virtual string Reason { get; private set; }
        
        /// <summary>
        /// 发送时间
        /// </summary>
        [Column("creation_time")]
        public virtual DateTime CreationTime { get; set; }

        /// <summary>
        /// 已发送
        /// </summary>
        public void HasSent()
        {
            State = MailState.Success;
        }

        /// <summary>
        /// 发送失败
        /// </summary>
        /// <param name="reason">失败原因</param>
        public void Failed(string reason)
        {
            State = MailState.Failed;
            Reason = reason;
        }
    }
}