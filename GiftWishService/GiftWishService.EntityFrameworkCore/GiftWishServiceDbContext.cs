﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Volo.Abp.EntityFrameworkCore;

namespace GiftWishService.EntityFrameworkCore
{
    /// <summary>
    /// 用户服务数据库上下文
    /// </summary>
    public class GiftWishServiceDbContext : AbpDbContext<GiftWishServiceDbContext>
    {
        public GiftWishServiceDbContext(DbContextOptions<GiftWishServiceDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 配置数据库实体
        }
    }

    public class LeoGeminiDbContextFactory : IDesignTimeDbContextFactory<GiftWishServiceDbContext>
    {
        public GiftWishServiceDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<GiftWishServiceDbContext>()
                .UseNpgsql(configuration.GetConnectionString("Default"));

            return new GiftWishServiceDbContext(builder.Options);
        }

        public static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);
            return builder.Build();
        }
    }
}