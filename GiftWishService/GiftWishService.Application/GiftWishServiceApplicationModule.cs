﻿using LeoGemini.Service.Common.Module;
using LeoGemini.Service.Infrastructure;
using GiftWishService.Domain;
using GiftWishService.EntityFrameworkCore;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace GiftWishService.Application
{
    [DependsOn(
        typeof(GiftWishServiceDomainModule),
        typeof(GiftWishServiceEntityFrameworkCoreModule),
        typeof(AbpAutoMapperModule),
        typeof(LeoGeminiFreeRedisModule),
        typeof(LeoGeminiServiceInfrastructureModule))]
    public class GiftWishServiceApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddProfile<GiftWishServiceAutoMapperProfile>(validate: true);
            });
        }
    }
}