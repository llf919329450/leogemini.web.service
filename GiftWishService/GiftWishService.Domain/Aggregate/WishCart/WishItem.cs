﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Values;

namespace GiftWishService.Domain.Aggregate.WishCart
{
    [Table("tb_wish_cart_item")]
    public class WishItem : ValueObject
    {
        /// <summary>
        /// 商品id
        /// </summary>
        [Column("goods_id")]
        public virtual string GoodsId { get; internal set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return GoodsId;
        }
    }
}