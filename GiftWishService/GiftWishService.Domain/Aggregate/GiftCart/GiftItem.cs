﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using LeoGemini.Service.Infrastructure.Handler.Exception;
using Volo.Abp.Domain.Values;

namespace GiftWishService.Domain.Aggregate.GiftCart
{
    /// <summary>
    /// 礼品袋条目
    /// </summary>
    [Table("tb_gift_cart_item")]
    public class GiftItem : ValueObject
    {
        /// <summary>
        /// 商品id
        /// </summary>
        [Column("goods_id")]
        public virtual string GoodsId { get; internal set; }
        
        /// <summary>
        /// 商品库存id
        /// </summary>
        [Column("sku_id")]
        public virtual Guid SkuId { get; internal set; }
        
        /// <summary>
        /// 单价
        /// </summary>
        [Column("price")]
        public virtual decimal Price { get; internal set; }
        
        /// <summary>
        /// 数量
        /// </summary>
        [Column("num")]
        public virtual int Num { get; internal set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return GoodsId;
            yield return SkuId;
            yield return Price;
            yield return Num;
        }

        /// <summary>
        /// 添加商品数量
        /// </summary>
        public void AddNum()
        {
            Num += 1;
        }

        /// <summary>
        /// 减少商品数量
        /// </summary>
        public void SubNum()
        {
            if (Num - 1 < 0)
                throw new BadRequestException(ErrorMessage.CART_ITEM_NUM_ERROR);
            Num -= 1;
        }
    }
}