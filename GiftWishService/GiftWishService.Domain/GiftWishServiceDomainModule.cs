﻿using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace GiftWishService.Domain
{
    [DependsOn(typeof(AbpIdentityDomainModule))]
    public class GiftWishServiceDomainModule : AbpModule
    {
    }
}