﻿namespace GiftWishService.Domain
{
    public static class ErrorMessage
    {
        public const string CART_ITEM_NUM_ERROR = "商品数量不能小于0";
        public const string CART_ITEM_ALREADY_EXISTS = "条目已存在";
        public const string CART_ITEM_NOT_EXISTS = "条目不存在";
    }
}