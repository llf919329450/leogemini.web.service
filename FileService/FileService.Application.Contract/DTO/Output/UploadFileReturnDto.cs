﻿namespace FileService.Application.Contract.DTO.Output
{
    public class UploadFileReturnDto
    {
        /// <summary>
        /// 文件id
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// url地址
        /// </summary>
        public string Url { get; set; }
        
        /// <summary>
        /// ContentType
        /// </summary>
        public string Mime { get; set; }
        
        /// <summary>
        /// Sha1值
        /// </summary>
        public string Sha1 { get; set; }
        
        /// <summary>
        /// Md5值
        /// </summary>
        public string Md5 { get; set; }
    }
}