﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FileService.Application.Contract.DTO.Output;
using Microsoft.AspNetCore.Http;

namespace FileService.Application.Contract.IAppService
{
    public interface IFileAppService
    {
        Task<ICollection<UploadFileReturnDto>> UploadAsync(List<IFormFile> files);
    }
}