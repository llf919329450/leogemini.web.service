﻿using AutoMapper;
using FileService.Application.Contract.DTO.Output;
using FileService.Domain.Aggregate.File;

namespace FileService.Application
{
    public class FileServiceAutoMapperProfile : Profile
    {
        public FileServiceAutoMapperProfile()
        {
            CreateMap<File, UploadFileReturnDto>();
        }
    }
}