﻿using LeoGemini.Service.Common.Module;
using LeoGemini.Service.Infrastructure;
using FileService.Domain;
using FileService.EntityFrameworkCore;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace FileService.Application
{
    [DependsOn(
        typeof(FileServiceDomainModule),
        typeof(FileServiceEntityFrameworkCoreModule),
        typeof(AbpAutoMapperModule),
        typeof(LeoGeminiFreeRedisModule),
        typeof(LeoGeminiConsulModule),
        typeof(LeoGeminiStaticFileModule),
        typeof(LeoGeminiServiceInfrastructureModule))]
    public class FileServiceApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddProfile<FileServiceAutoMapperProfile>(validate: true);
            });
        }
    }
}