﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.Modeling;
using File = FileService.Domain.Aggregate.File.File;

namespace FileService.EntityFrameworkCore
{
    /// <summary>
    /// 用户服务数据库上下文
    /// </summary>
    public class FileServiceDbContext : AbpDbContext<FileServiceDbContext>
    {
        public DbSet<File> File { get; set; }
        
        public FileServiceDbContext(DbContextOptions<FileServiceDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 配置数据库实体
            modelBuilder.Entity<File>(f =>
            {
                f.ConfigureByConvention();
            });
        }
    }

    public class LeoGeminiDbContextFactory : IDesignTimeDbContextFactory<FileServiceDbContext>
    {
        public FileServiceDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<FileServiceDbContext>()
                .UseNpgsql(configuration.GetConnectionString("Default"));

            return new FileServiceDbContext(builder.Options);
        }

        public static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);
            return builder.Build();
        }
    }
}