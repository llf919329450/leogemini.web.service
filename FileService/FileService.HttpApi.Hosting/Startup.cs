using FileService.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FileService.HttpApi.Hosting
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplication<FileServiceHttpApiHostingModule>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, FileServiceDbContext context)
        {
            app.InitializeApplication();
            context.Database.Migrate();
        }
    }
}