﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FileService.Application.Contract.IAppService;
using LeoGemini.Service.Infrastructure.Constract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FileService.HttpApi.Hosting.Controller
{
    [Route("api/v1/files")]
    public class FilesController : ApiResponseController
    {
        private readonly IFileAppService _fileAppService;

        public FilesController(IFileAppService fileAppService)
        {
            _fileAppService = fileAppService;
        }
        
        /// <summary>
        /// 上传文件
        /// </summary>
        [HttpPost("upload")]
        public async Task<object> Upload(List<IFormFile> files)
        {
            return await _fileAppService.UploadAsync(files);
        }
    }
}