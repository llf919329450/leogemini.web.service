﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LeoGemini.Service.Infrastructure.Extension;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;

namespace FileService.Domain.Aggregate.File
{
    [Table("tb_file")]
    public class File : AggregateRoot<Guid>, IHasCreationTime
    {
        /// <summary>
        /// ORM框架使用
        /// </summary>
        protected File()
        {
            
        }

        public File(Guid id)
        {
            Id = id;
        }
        
        public File(Guid id, IFormFile file, string webContentRoot) : this(id)
        {
            tempFile = file;
            _webContentRoot = webContentRoot;
            Mime = file.ContentType;
            Sha1 = file.GetSha1();
            Md5 = file.GetMd5();
            CreationTime = DateTime.Now;
            Year = CreationTime.Year;
            Month = CreationTime.Month;
            Day = CreationTime.Day;
            Path = $"/static/files/{Year}/{Month}/{Day}";
            FileName = $"{Sha1}.{tempFile.GetSuffix()}";
        }
        
        /// <summary>
        /// 文件在服务器上的绝对路径
        /// </summary>
        [Column("abs_path")]
        public virtual string AbsPath { get; private set; }
        
        /// <summary>
        /// 文件路径
        /// </summary>
        [Column("path")]
        public virtual string Path { get; private set; }

        /// <summary>
        /// 文件名
        /// </summary>
        [Column("file_name")]
        public virtual string FileName { get; set; }

        /// <summary>
        /// url地址
        /// </summary>
        public virtual string Url => Path + "/" + FileName;

        /// <summary>
        /// mime类型
        /// </summary>
        [Column("mime")]
        public virtual string Mime { get; set; }
        
        /// <summary>
        /// Sha1值
        /// </summary>
        [Column("sha1")]
        public virtual string Sha1 { get; private set; }
        
        /// <summary>
        /// Md5值
        /// </summary>
        [Column("md5")]
        public virtual string Md5 { get; private set; }
        
        /// <summary>
        /// 年
        /// </summary>
        [Column("year")]
        public virtual int Year { get; private set; }
        
        /// <summary>
        /// 月
        /// </summary>
        [Column("month")]
        public virtual int Month { get; private set; }
        
        /// <summary>
        /// 日
        /// </summary>
        [Column("day")]
        public virtual int Day { get; private set; }
        
        private IFormFile tempFile;
        
        private string _webContentRoot;
        
        /// <summary>
        /// 上传时间
        /// </summary>
        public virtual DateTime CreationTime { get; set; }
        
        /// <summary>
        /// 文件保存到本地
        /// </summary>
        public void SaveToLocal()
        {
            AbsPath = tempFile.SaveTo(_webContentRoot + Path).Replace('/', '\\');
        }
    }
}