﻿using System;
using System.Linq.Expressions;
using FileService.Domain.Aggregate.File;
using Volo.Abp.Specifications;

namespace FileService.Domain.Specification
{
    public class GetFileBySha1Md5Specification : Specification<File>
    {
        private readonly string fileHash;

        public GetFileBySha1Md5Specification(string hash)
        {
            fileHash = hash;
        }

        public override Expression<Func<File, bool>> ToExpression()
        {
            return file => file.Sha1 == fileHash || file.Md5 == fileHash;
        }
    }
}