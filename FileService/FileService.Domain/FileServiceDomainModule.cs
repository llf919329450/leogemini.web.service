﻿using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace FileService.Domain
{
    [DependsOn(typeof(AbpIdentityDomainModule))]
    public class FileServiceDomainModule : AbpModule
    {
    }
}