﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;

namespace LeoGemini.Service.Gateway
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://*:5200")
                .ConfigureAppConfiguration((ctx, config) =>
                {
                    config
                        .SetBasePath(ctx.HostingEnvironment.ContentRootPath)
                        .AddJsonFile("Ocelot.json", false, true)
                        .AddEnvironmentVariables();
                })
                .ConfigureServices(services =>
                {
                    services.AddAuthentication()
                        .AddJwtBearer("VerifyUser", x =>
                        {
                            x.Authority = "User";
                            x.Audience = "LeoGemini.Client";
                        });
                    
                    services
                        .AddOcelot()
                        .AddConsul();
                })
                .Configure(app =>
                {
                    app.UseOcelot();
                });
    }
}