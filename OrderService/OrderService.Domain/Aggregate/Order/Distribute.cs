﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Values;

namespace OrderService.Domain.Aggregate.Order
{
    /// <summary>
    /// 配送地址信息
    /// </summary>
    [Table("tb_order_distribute")]
    public class Distribute : ValueObject
    {
        /// <summary>
        /// 联系人
        /// </summary>
        [Column("contact")]
        public virtual string Contact { get; internal set; }
        
        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("phone_num")]
        public virtual string PhoneNum { get; internal set; }
        
        /// <summary>
        /// 省份
        /// </summary>
        [Column("province")]
        public virtual string Province { get; internal set; }
        
        /// <summary>
        /// 城市
        /// </summary>
        [Column("city")]
        public virtual string City { get; internal set; }
        
        /// <summary>
        /// 地区
        /// </summary>
        [Column("region")]
        public virtual string Region { get; internal set; }
        
        /// <summary>
        /// 详情
        /// </summary>
        [Column("details")]
        public virtual string Details { get; internal set; }
        
        /// <summary>
        /// 快递单号
        /// </summary>
        [Column("ship_no")]
        public virtual string ShipNo { get; internal set; }
        
        /// <summary>
        /// 发货时间
        /// </summary>
        [Column("distributed_time")]
        public virtual DateTime DistributedTime { get; internal set; }
        
        /// <summary>
        /// 签收时间
        /// </summary>
        [Column("signed_time")]
        public virtual DateTime SignedTime { get; internal set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Contact;
            yield return PhoneNum;
            yield return Province;
            yield return City;
            yield return Region;
            yield return Details;
            yield return ShipNo;
            yield return DistributedTime;
        }
    }
}