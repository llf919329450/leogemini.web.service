﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Values;

namespace OrderService.Domain.Aggregate.Order
{
    /// <summary>
    /// 订单条目
    /// </summary>
    [Table("tb_order_item")]
    public class OrderItem : ValueObject
    {
        /// <summary>
        /// 商品id
        /// </summary>
        [Column("goods_id")]
        public virtual string GoodsId { get; internal set; }
        
        /// <summary>
        /// 库存单元id
        /// </summary>
        [Column("sku_id")]
        public virtual Guid SkuId { get; internal set; }
        
        /// <summary>
        /// 单价
        /// </summary>
        [Column("price")]
        public virtual decimal Price { get; internal set; }
        
        /// <summary>
        /// 数量
        /// </summary>
        [Column("num")]
        public virtual int Num { get; internal set; }
        
        public decimal PartPrice => Num * Price;

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return GoodsId;
            yield return SkuId;
            yield return Price;
            yield return Num;
        }
    }
}