﻿using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace OrderService.Domain
{
    [DependsOn(typeof(AbpIdentityDomainModule))]
    public class OrderServiceDomainModule : AbpModule
    {
    }
}