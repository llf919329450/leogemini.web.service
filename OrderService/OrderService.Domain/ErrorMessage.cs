﻿namespace OrderService.Domain
{
    public static class ErrorMessage
    {
        public const string SHIP_NO_IS_NULL = "快递单号不合法";
        public const string ORDER_STATE_ERROR = "订单状态不正确";
        public const string CAN_NOT_REPEAT_ACTION = "已操作过的订单无法重复操作";
        public const string CAN_NOT_REWRITE_CLOSED = "已关闭的订单无法进行其他操作";
        
    }
}