﻿namespace OrderService.Domain.Shared.Enum
{
    /// <summary>
    /// 订单状态
    /// </summary>
    public enum OrderState
    {
        /// <summary>
        /// 待支付
        /// </summary>
        WaitingForPay,
        
        /// <summary>
        /// 待发货
        /// </summary>
        WaitingForDistribute,
        
        /// <summary>
        /// 待签收
        /// </summary>
        WaitingForSigned,
        
        /// <summary>
        /// 待确认
        /// </summary>
        WaitingForConfirm,
        
        /// <summary>
        /// 订单完成
        /// </summary>
        Done,
        
        /// <summary>
        /// 订单关闭
        /// </summary>
        Closed,
        
        /// <summary>
        /// 已退货
        /// </summary>
        Rejected
    }
}