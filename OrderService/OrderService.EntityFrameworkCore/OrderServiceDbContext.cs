﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Volo.Abp.EntityFrameworkCore;

namespace OrderService.EntityFrameworkCore
{
    /// <summary>
    /// 用户服务数据库上下文
    /// </summary>
    public class OrderServiceDbContext : AbpDbContext<OrderServiceDbContext>
    {
        public OrderServiceDbContext(DbContextOptions<OrderServiceDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 配置数据库实体
        }
    }

    public class LeoGeminiDbContextFactory : IDesignTimeDbContextFactory<OrderServiceDbContext>
    {
        public OrderServiceDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<OrderServiceDbContext>()
                .UseNpgsql(configuration.GetConnectionString("Default"));

            return new OrderServiceDbContext(builder.Options);
        }

        public static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);
            return builder.Build();
        }
    }
}