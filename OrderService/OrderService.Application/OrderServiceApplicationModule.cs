﻿using LeoGemini.Service.Common.Module;
using LeoGemini.Service.Infrastructure;
using OrderService.Domain;
using OrderService.EntityFrameworkCore;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace OrderService.Application
{
    [DependsOn(
        typeof(OrderServiceDomainModule),
        typeof(OrderServiceEntityFrameworkCoreModule),
        typeof(AbpAutoMapperModule),
        typeof(LeoGeminiFreeRedisModule),
        typeof(LeoGeminiServiceInfrastructureModule))]
    public class OrderServiceApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddProfile<OrderServiceAutoMapperProfile>(validate: true);
            });
        }
    }
}